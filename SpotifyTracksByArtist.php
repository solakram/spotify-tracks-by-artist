<?php
$spotifyTracksByArtist = new SpotifyTracksByArtist();
$spotifyTracksByArtist->execute();

class SpotifyTracksByArtist {

	private $token;
	private $artistId;
	private $artistName;
	private $playlistId;
	private $addRemixesFromOtherArtists;
	const SAVE_RESULTS = false;

	public function execute() {
		$this->readConfigFile();
		$albums = $this->getAlbums();
		$tracks = array_merge($this->getTracksFromAlbums($albums), $this->getRemixTracks());
		$this->addTracksToPlaylist($tracks);
	}

	private function readConfigFile() {
		if(!file_exists("config.json")) {
			throw new Exception('Config file (config.json) does not exist');
		}
		$config = json_decode(file_get_contents("config.json"), true);
		$this->token = $config['token'];
		$this->artistId = $config['artistId'];
		$this->artistName = $config['artistName'];
		$this->playlistId = $config['playlistId'];
		$this->addRemixesFromOtherArtists = (bool) $config['addRemixesFromOtherArtists'];
	}

	private function getAlbums() {
		$albums = [];
		$url = 'https://api.spotify.com/v1/artists/'.$this->artistId.'/albums';
		do {
			$curl = 'curl -X "GET" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.$this->token.'"';
			$response = json_decode(shell_exec($curl), true);
			$albums = array_merge($albums, $response['items']);
			$url = array_key_exists('next', $response) ? $response['next'] : null;
		} while ($url);
		if (SpotifyTracksByArtist::SAVE_RESULTS) {
			file_put_contents('albums.json', json_encode($albums));
		}

		return $albums;
	}

	private function getTracksFromAlbums($albums) {
		$albumIds = [];
		foreach ($albums as $album) {
			$albumIds[] = $album['id'];
		}
		$requestCounts = 0;
		$tracks = [];
		foreach (array_chunk($albumIds, 5) as $albumIdsChunk) {
			$url = 'https://api.spotify.com/v1/albums?ids='.implode('%2C', $albumIdsChunk);
			$curl = 'curl -X "GET" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.$this->token.'"';
			$response = json_decode(shell_exec($curl), true);
			foreach ($response['albums'] as $album) {
				$tracks = array_merge($tracks, $this->getTracksFromAlbum($album));
			}
			$requestCounts++;
		}

		$filteredTracks = [];
		foreach ($tracks as $trackKey => $track) {
			foreach ($track['artists'] as $artist) {
				if ($artist['id'] == $this->artistId
					&& ($this->addRemixesFromOtherArtists || !$this->isRemixFromOtherArtist($track))
				) {
					$filteredTracks[] = $track;
					continue;
				}
			}
		}
		if (SpotifyTracksByArtist::SAVE_RESULTS) {
			file_put_contents('tracks.json', json_encode($filteredTracks));
		}

		return $filteredTracks;
	}

	private function isRemixFromOtherArtist($track) {
		$name = $track["name"];
		return (strpos(strtolower($name), 'remix') !== false) && (strpos(strtolower($name), $this->artistName) === false);
	}

	private function getTracksFromAlbum($album) {
		$tracks = $album['tracks']['items'];
		$url = array_key_exists('next', $album['tracks']) ? $album['tracks']['next'] : null;
		while ($url) {
			$curl = 'curl -X "GET" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.$this->token.'"';
			$response = json_decode(shell_exec($curl), true);
			$tracks = array_merge($tracks, $response['items']);
			$url = array_key_exists('next', $response) ? $response['next'] : null;
		}

		return $tracks;
	}

	private function getRemixTracks() {
		$remixTracks = [];
		if($this->artistName) {
			$url = 'https://api.spotify.com/v1/search?q='.urlencode($this->artistName.' Remix').'&type=track';
			do {
				$curl = 'curl -X "GET" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.$this->token.'"';
				$response = json_decode(shell_exec($curl), true);
				if (!array_key_exists('tracks', $response)) {
					throw new Exception("Search request returned wrong result: \n" . json_encode($response,  JSON_PRETTY_PRINT));
				}
				$remixTracks = array_merge($remixTracks, $response['tracks']['items']);
				$url = array_key_exists('next', $response['tracks']) ? $response['tracks']['next'] : null;
			} while ($url);
			foreach ($remixTracks as $trackKey => $track) {
				if (strstr($track['name'], $this->artistName) === false) {
					unset($remixTracks[$trackKey]);
				}
			}
			if (SpotifyTracksByArtist::SAVE_RESULTS) {
				file_put_contents('remixTracks.json', json_encode($remixTracks));
			}
		}

		return $remixTracks;
	}

	private function addTracksToPlaylist($tracks) {
		$trackUris = [];
		$trackNames = [];
		foreach ($tracks as $track) {
			if (!self::isDuplicate($track['name'], $track['uri'], $trackNames, $trackUris)) {
				$trackUris[$track['uri']] = true;
				$trackNames[$track['name']] = true;
			}
		}

		$trackUris = array_keys($trackUris);
		foreach (array_chunk($trackUris, 100) as $trackUrisChunk) {
			$url = 'https://api.spotify.com/v1/users/1191338807/playlists/'.$this->playlistId.'/tracks';
			$curl = 'curl -X "POST" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.$this->token.'"';
			$body = ['uris' => $trackUrisChunk];
			$curl .= ' -d \''.json_encode($body).'\'';
			$response = json_decode(shell_exec($curl), true);
			if (array_key_exists('error', $response)) {
				print("Error occurred while adding track\n" . json_decode($response, JSON_PRETTY_PRINT));
			}
		}
	}

	private static function isDuplicate($trackName, $uri, $trackNames, $trackUris) {
		return array_key_exists($uri, $trackUris) || array_key_exists($trackName, $trackNames);
	}

}
